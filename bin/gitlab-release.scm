#!/usr/bin/guile \
--no-auto-compile -e main -s
!#
(use-modules
 (json)
 (ice-9 curried-definitions)
 (ice-9 getopt-long)
 (ice-9 format)
 (ice-9 ftw)
 (ice-9 match)
 (ice-9 iconv)
 (ice-9 regex)
 (ice-9 textual-ports)
 (rnrs bytevectors)
 (srfi srfi-1)
 (srfi srfi-9)
 (srfi srfi-98)
 (gnutls)
 (web client)
 (web response))

(define params '(name codename tag package-dir aws-bucket gitlab-release-url gitlab-release-description-template gitlab-token))
(define (param-record-print param-record port)
  (let* ((param-record-type (record-type-descriptor param-record))
         (param-access (lambda (param) ((record-accessor param-record-type param) param-record))))
    (write-char #\[ port)
    (display 
     (fold (lambda (param str)
             (string-append str " " (param-access param)))
           (param-access (car params))
           (cdr params))
     port)
    (write-char #\] port)))
(define param-record-type (make-record-type "param-record" params param-record-print))

(define (param-get param-record param)
  ((record-accessor param-record-type param) param-record))

(define (param-option-spec)
  (map (lambda (param) `(,param (value #t) (required? #t))) params))
(define (param-options args)
  (getopt-long args (param-option-spec)))
(define (param-options->params options)
  (apply (record-constructor param-record-type) 
   (map (lambda (param) (option-ref options param "N/A")) params)))
(define (make-params args)
  (param-options->params (param-options args)))

(define (package-paths params)
  (let ((package-dir (param-get params 'package-dir)))
    (define (enter? name stat result) #t)
    (define (down name stat result) result)
    (define (up name stat result) result)
    (define (skip name stat result) result)
    (define (error name stat errno result) result)
    (define (leaf name stat result) (append result `(,name)))
    (file-system-fold enter? leaf down up skip error '() package-dir)))

(define (package-path-os path)
  (string-join (reverse (list-head (reverse (string-split (dirname path) #\/)) 2)) "/"))
(define (package-path-pkg path)
  (basename path))
(define (package-path-name path)
  (string-join (list (package-path-os path) (package-path-pkg path)) "/"))
(define (aws-url aws-bucket)
  (format #f "https://~a.s3.amazonaws.com" aws-bucket))
(define (package-url aws-bucket path)
  (string-join (list (aws-url aws-bucket) (package-path-os path) (package-path-pkg path)) "/"))

(define ((aws-upload-package aws-bucket) path)
  (system (format #f "aws s3 cp ~a s3://~a/~a" path aws-bucket (package-path-name path))))
(define (aws-upload-packages params)
  (for-each (aws-upload-package (param-get params 'aws-bucket)) (package-paths params)))

(define (gitlab-release-headers params)
  (let* ((gitlab-token (param-get params 'gitlab-token)))
    `((Content-Type . "application/json")
      (PRIVATE-TOKEN . ,gitlab-token))))

(define ((gitlab-release-link aws-bucket)path)
  (list (cons "name" (package-path-name path))
        (cons "url" (package-url aws-bucket path))))
(define (gitlab-release-links params)
  (let* ((package-paths (package-paths params))
         (aws-bucket (param-get params 'aws-bucket)))
    (list->vector (map (gitlab-release-link aws-bucket) package-paths))))
(define (gitlab-release-assets params)
  (list (cons "links" (gitlab-release-links params))))

(define gitlab-release-description-params '(codename tag))
(define (gitlab-release-description-template params)
   (call-with-input-file (param-get params 'gitlab-release-description-template) get-string-all))
(define ((gitlab-release-description-instantiate params) param template)
  (regexp-substitute #f
                     (string-match (format #f "\\{\\{ ~a \\}\\}" param) template)
                     'pre (param-get params param) 'post))
(define (gitlab-release-description params)
  (fold (gitlab-release-description-instantiate params)
        (gitlab-release-description-template params)
        gitlab-release-description-params))

(define (gitlab-release-request-body params)
  (let *((name (param-get params 'tag))
         (tag_name (param-get params 'tag))
         (description (gitlab-release-description params))
         (assets (gitlab-release-assets params)))
    (string->utf8 (scm->json-string `(("name"        . ,name)
                                      ("tag_name"    . ,tag_name)                                      
                                      ("description" . ,description)
                                      ("assets"      . ,assets))
                                    #:solidus #t
                                    #:unicode #t
                                    #:validate #t))))
  
(define (gitlab-release params)
  (let* ((gitlab-release-url (param-get params 'gitlab-release-url))
         (gitlab-release-headers (gitlab-release-headers params))
         (gitlab-release-request-body (gitlab-release-request-body params)))
    (http-request gitlab-release-url
                  #:method  'POST
                  #:headers gitlab-release-headers
                  #:decode-body? #f
                  #:body    gitlab-release-request-body)))

(define (main args)
  (let* ((params (make-params args)))
    (begin
      (aws-upload-packages params)
      (gitlab-release params))))
