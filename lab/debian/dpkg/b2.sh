#!/bin/bash

version=$1

mkdir b2
wget https://github.com/boostorg/build/archive/${version}.tar.gz 
mkdir b2-${version}-src
tar xf ${version}.tar.gz -C b2-${version}-src --strip-components 1 
cd b2-${version}-src
./bootstrap.sh 
./b2 install --prefix=../b2/usr
cd ..
rm -rf b2-${version}-src
rm ${version}.tar.gz

mkdir b2/etc
cat >b2/etc/site-config.jam <<EOF
using gcc ;
EOF

mkdir -p b2/DEBIAN
cat >b2/DEBIAN/control <<EOF
Package: b2
Description: c++ build tool
Architecture: amd64
Maintainer: John Eivind Helset
Priority: optional
Version: ${version}
EOF

dpkg-deb --build b2 b2.deb

rm -rf b2
