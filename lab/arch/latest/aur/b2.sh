#!/bin/sh
git clone https://aur.archlinux.org/boost-build
pushd boost-build
makepkg -si --noconfirm --skipchecksums --needed
echo "using gcc ;" | sudo tee -a /etc/site-config.jam > /dev/null ;
popd
rm -rf boost-build
