#!/bin/sh
git clone https://aur.archlinux.org/guile-json
pushd guile-json
makepkg -si --noconfirm --skipchecksums --needed
popd
rm -rf guile-json
