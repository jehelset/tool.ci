#!/bin/sh
git clone https://aur.archlinux.org/range-v3-git
pushd range-v3-git
makepkg -si --noconfirm --skipchecksums --needed
popd
rm -rf range-v3-git
